# CSE 306 Fall 2017

This is the private repository for CSE 306.

# Retrieving Labs

To get the read-only labs, perform the following steps:

```sh
$ git remote add lab<#> <git-repository>
$ git fetch lab<#>
$ git merge -m "Merging lab<#>" lab<#>/lab<#>
```

# Authors
 - Freeman Lou
 - Nathan Wood