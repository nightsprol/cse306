// On-disk file system format.
// Both the kernel and user programs use this header file.


#define ROOTINO 1  // root i-number
#define BSIZE 512  // block size

// Disk layout:
// [ boot block | sb block | log | inode blocks | bit map | data blocks | ... ]
//                                \------------Block Group 1-----------/  ...
//
// mkfs computes the super block and builds an initial file system. The
// super block describes the disk layout:
struct superblock {
  uint size;         // Size of file system image (blocks)
  uint nblocks;      // Number of data blocks
  uint ninodes;      // Number of inodes.
  uint nlog;         // Number of log blocks
  uint logstart;     // Block number of first log block
  uint bgstart;      // Block number of first block group block
  uint bgsize;       // Number of blocks in each block group
};

#define NINODEBLOCKS 4                                      // Number of inode blocks per group
#define NDATABLOCKS ((NDIRECT+1) * IPB * NINODEBLOCKS)      // Number of data blocks per group
#define NBITMAP     (NDATABLOCKS/(BSIZE*8) + 1)             // Number of bitmap blocks per group
#define BGSIZE      (NBITMAP + NDATABLOCKS + NINODEBLOCKS)  // Number of blocks per group

#define NDIRECT 12
#define NINDIRECT (BSIZE / sizeof(uint))
#define MAXFILE (NDIRECT + NINDIRECT)

// On-disk inode structure
struct dinode {
  short type;           // File type
  short major;          // Major device number (T_DEV only)
  short minor;          // Minor device number (T_DEV only)
  short nlink;          // Number of links to inode in file system
  uint size;            // Size of file (bytes)
  uint addrs[NDIRECT+1];   // Data block addresses
};

#define IPB    (BSIZE / sizeof(struct dinode))    // Inodes per block.
#define IPG    (NINODEBLOCKS * IPB)               // Inodes per group.

// Block group containing block b
#define BBLOCKGROUP(b, sb) (((b) - sb.bgstart) / sb.bgsize)

// Block group containing inode i
#define IBLOCKGROUP(i) ((i) / IPG)

// Block containing inode i (takes into account block groups)
#define IBLOCK(i, sb) (sb.bgstart + (sb.bgsize * IBLOCKGROUP(i)) + ((i) % IPG / IPB))

// Bitmap bits per block
#define BPB           (BSIZE*8)

// Block of free map containing bit for block b (takes block groups into account)
#define BBLOCK(b, sb) (sb.bgstart + (sb.bgsize * BBLOCKGROUP(b, sb)) + ((b - sb.bgstart) % sb.bgsize / BPB) + NINODEBLOCKS)

// Directory is a file containing a sequence of dirent structures.
#define DIRSIZ 14

struct dirent {
  ushort inum;
  char name[DIRSIZ];
};

