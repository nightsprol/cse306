#include "types.h"
#include "defs.h"
#include "kalloc.h"
#include "traps.h"
#include "x86.h"
#include "proc.h"
#include "mmu.h"
#include "memlayout.h"
#include "spinlock.h"

// Forward declarations
int extend_stack(struct proc*, pte_t*);
void copy_on_write(uint, pte_t*);

/*
  Handles a page fault.

  If the fault occurs within the stack guard,
  the stack is extended by a page.

  If the fault is on a write and the page is RO and COW,
  a new copy of the page is allocated and mapped.
*/
void
pagefault(struct trapframe *tf)
{
  uint va = rcr2();
  pte_t *pte;
  uint cpu_id = cpuid();
  struct proc *curproc = myproc();

  // Check if the VA falls into the first page (segfault)
  if(va >= 0 && va < PGSIZE) {
    cprintf("segmentation fault: pid %d %s: on cpu %d eip 0x%x\n",
            curproc->pid, curproc->name, cpu_id, tf->eip);
    curproc->killed = 1;
    return;
  }
  
  // Find the PTE for the faulted VA
  if((pte = walkpgdir(curproc->pgdir, (void*)va, 0)) == 0)
    panic("pagefault: failed to find pte");
    
  // These conditions are used a couple times and aren't very obvious,
  // so we precalculate them to make the code more readable
  uint is_cow = *pte & PTE_COW;
  uint is_write_on_ro = !(tf->err & FEC_WR);
  uint is_kernel = (curproc == 0 || (tf->cs&3) == 0);
  
  // Handle kernel page fault
  if(is_kernel && (!is_cow || is_write_on_ro)){
    cprintf("pagefault: in kernel on cpu %d eip %x (cr2=0x%x)\n",
            cpu_id, tf->eip, va);
    panic("pagefault: inside kernel");
  }
  
  // Check if the requested VA falls inside the stack guard
  if(va >= (uint)curproc->stack_guard && va < (uint)curproc->stack_guard + PGSIZE) {
    if(extend_stack(curproc, pte) < 0) {
      curproc->killed = 1;
      return;
    }
    copy_on_write(va, pte);
    return;
  }

  // If it's a read error just kill the process like normal
  if(is_write_on_ro) {
    cprintf("pagefault: read: pid %d %s: trap %d err %d on cpu %d eip 0x%x addr 0x%x--kill proc\n",
            curproc->pid, curproc->name, tf->trapno, tf->err, cpu_id, tf->eip, va);
    curproc->killed = 1;
    return;
  }
    
  // If the process tried to write to a RO page that is not COW, kill it like normal
  if(!is_cow) {
    cprintf("pagefault: write: pid %d %s: trap %d err %d on cpu %d eip 0x%x addr 0x%x--kill proc\n",
            curproc->pid, curproc->name, tf->trapno, tf->err, cpu_id, tf->eip, va);
    curproc->killed = 1;
    return;
  }
  
  // Perform copy-on-write
  copy_on_write(va, pte);
}

// Performs copy-on-write (COW) for the specified VA
// and its pre-fetched PTE
void
copy_on_write(uint va, pte_t *pte)
{
  // Get the physical address from the PTE, this is used to get the run and map the page
  uint pa = PTE_ADDR(*pte);
  
  if(kmem.use_lock)
    acquire(&kmem.run_lock);
    
  struct run *run = p2run(pa);

  if(!run)
    panic("pagefault: could not find the run associated with the VA");

  if(run->reference_count == 0)
    panic("pagefault: run reference count was 0");
  
  // Perform copy-on-write
  if(run->reference_count > 1) {
    char* mem;
      
    if((mem = kalloc()) == 0)
      panic("pagefault: out of mem in COW");

    // Copy the data from the page
    memmove(mem, (char*)P2V(pa), PGSIZE);

    // Set all bits of the old PA to 0 then mask in the new PA
    *pte &= 0xFFF;
    *pte |= V2P(mem);
                  
    // Decrement the reference count in the original page
    if(decrement_run(pa) != 0)
      panic("pagefault: could not decrement the run");
      
  } else {
    // This is the only thread referencing the page, just remove COW flag
    *pte &= ~PTE_COW;
  }

  if(kmem.use_lock)
    release(&kmem.run_lock);

  // Set the page to writeable and invalidate the entry in the TLB
  *pte |= PTE_W;
  invlpg((void*)va);
}

// Extend the stack by a single page
// Returns -1 and prints an error message if something went wrong
// Otherwise returns 0
int
extend_stack(struct proc *curproc, pte_t *pte)
{
  char *new_page;
  
  // Make sure that stack will not exceed max size
  if(curproc->stack_guard == curproc->stack_end) {
    cprintf("extend_stack: process stack exceeded max size\n");
    return -1;
  }

  if((new_page = kalloc()) == 0) {
    cprintf("extend_stack: out of memory\n");
    return -1;
  }

  // Allow the user program to write to the page
  *pte |= PTE_U;
  *pte |= PTE_W;

  // The PTE has been modified so its entry in the TLB must be flushed
  invlpg(curproc->stack_guard);

  // Move the stack guard VA down by a page
  curproc->stack_guard -= PGSIZE;

  // Map the stack guard to the newly kalloc()'d page
  if(mappages(curproc->pgdir, curproc->stack_guard, PGSIZE, V2P(new_page), PTE_W) < 0) {
    cprintf("extend_stack: out of memory (2)\n");
    kfree(new_page);
    return -1;
  }

  return 0;
}
