#include "types.h"
#include "syscall.h"
#include "defs.h"

int wolfie(char *buf, uint size) {
  const uint WOLFIE_SIZE = sizeof(char) * 2789;
  char* wolfie_s = " ........................................IZ..................,...................\n ........................................+ZZZ................=Z~.................\n ........................................?ZZZZ:..............?ZZZ................\n ........................................7ZOOZZ?.............$ZZZZ+..............\n ........................................ZZOOOZZZ............ZZOOZZ+.............\n ........................................ZZOOOOZZZ...........ZZOOOZZ.............\n .......................................IZZOZ7OOZZZZZZZZZZZZZZOOOOOZZ............\n .............................=7ZZZZZZZZZZZO=.OOOZZZOOOOOZZZZZOOOOOZZ............\n ........................,ZZZZZZZZZZZZOZZZOO..+OOZZZZOOOOOOOOZZOOOOZZ............\n ..........................IZZZZOOOOOOOOOOOZ...OOOOOOZZOOOOOOOOZOOOZZ,...........\n ........................+ZZZZZZZZOOOOOOOOO,...OOOOOOOOOOOOOOOOOOOOZZ............\n ...................~ZZZZZZZZZOOOOOOOOOOOO$....7OOOOOOOOOOOOOOOOOOOZZ............\n ................=ZZZZZZZOOOOOOOOOOOOOOOOOZO,O$?OOOOOOOOOOOOOOOOOOOZZ:...........\n .............=ZZZZZZOOOOOOOOOOOOOOZZZZOOOOOOOOOOOOO$ZOOOOO?OOOOOOOOZZ...........\n ...........IZZZZZOOOOOOOO7+==================?ZOOOOO$===+$O=ZOOOOOOZZ:..........\n .........?ZZZZOOOOOOZ+++I77$$7?=====~,...=OOOOOOOOOOOZ==O7===OOOOOOOZ$..........\n ........$ZZZZOOOOOOOOOOOOOO$====~,....~7OOOOOOOOOOOOOO==7OOZ=OOOOOOOZZ..........\n ......ZZZZZZZZZOOOOOOOOO?==~.......,,+OOOOOOOOOO$?=ZOOZ==ZZO?=IOOOZZZZ..........\n .....+==~ZZZZOOOOOOOZ===,.....:=======IZOOOO7===+OOOOOOOO77OOOOOOOOZZ~..........\n .......=ZZZOOOOOOOI==...:=======~:==+OOOOO+====I+========7OZ==Z7IOOZZ7..........\n .....,ZZZOOOOOOO=======+?7$$ZZZ$$I+IOOOO$===========I$OOOI=====?OOOOZZ$.........\n ....=ZZZZZZOO7==IZOOOOOOOOOOOOOOOOOOOOOOOO+======7OZZZ:.:$OI====$OOOOZZZ$.......\n ...=ZZZZZZOOOOOOOZZZZZZZZZZZZZZZZZOOOOOOO=======ZO?..ZZZZZ.7$====+7$OOOZZZI.....\n ...Z,+ZZZOOOZZZZZZZZ$+,.......7ZZOOOZOOO7OO7=======IOO..ZZOZZI======IZOOOZZZ....\n ....:ZZZOZZZZZ$+..........=?:.ZZZZZZZOOOOO============ZOZOOZ.Z7====OOOOOOZZ?....\n ....ZZZZZZZ:.................IZZZZZZOOOO$====?777+======$~OZZZOO$===I=OOOZZ.....\n ...IZZZZ.....................ZZZ+,ZZOO$=?OOOOOOOOOOO$====I$:OO...O+Z?$ZZZZ......\n ...Z$............................ZZOOZOOOZZZZZZZZZZZOO$====OO.:$OZ$Z$ZZZ........\n ................................7ZZOZZZZZZZ7+===?$ZZZZOO+===I7..?$,OZZ7.........\n ...............................,ZZZZZ=..............IZZZO?====+IZOOZZ?..........\n ..............................=ZZ$....................ZZZOO?====IOOZZ...........\n ..............................7:........................ZZZOOOOOOZZZ............\n ..........................................................ZZZZZZZZ,.............\n ............................................................?$$7:...............\n";
  // length = 2789

  if(size < WOLFIE_SIZE || !buf)
    return -1;

  if(!memmove(buf, wolfie_s, WOLFIE_SIZE))
    return -1;
  
  return WOLFIE_SIZE;
}
