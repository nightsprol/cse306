#ifndef PAGEFAULT_H
#define PAGEFAULT_H

#include "traps.h"
#include "types.h"

uint pagefault(struct trapframe*);

#endif
