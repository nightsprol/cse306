# CSE 306 Lab 2 Readme

This document gives a high-level overview of the files that were modified or added in lab 2 and the reasons behind the changes. There are a good number of comments in the actual code itself that explains the implementation in further detail.

## Compiler Flags

There is a new flag called `COW_FORK` defined in `proc.h` that determines at compile time whether fork() and our page fault handler pagefault() perform copy-on-write operations.

## Copy-On-Write Implementation

### mmu.h

The bit mask for a page table entry's copy-on-write flag is defined in `mmu.h` and has the value 0x800 so that it uses one of the two available bits.

### kalloc.c

We created a header file for kalloc and moved `struct run`, `struct kmem`, and the kernel's instance of `kmem` to the header so other kernel files could view them, such as our page fault handler.

An extra lock was added to `struct kmem` for thread-safe accessing of runs. This was to lock around code blocks that were performing work based on the value of a run's reference count, such as copy-on-write work done in our page fault handler.

We also added four helpers to kalloc which have some brief documentation in the file itself. Two take a virtual or physical address as an argument and return the associated run, and the other two perform atomic incrementing and decrementing of a run's reference count.

### proc.c

We modified fork() to check whether `COW_FORK` was set. If it is, it will use our new cowuvm() function instead of the original copyuvm() function.

### vm.c

In `vm.c` we have our implementation of cowuvm(). The implementation will map all pages as copyuvm() did but not create copies, and it will increment each run's reference count. If the page was RW, it will set it to RO and COW. This guarantees that there will still be a page fault on write so that we can handle it appropriately.

The only allocation that is performed in cowuvm() is for the kernel's address space, the new process's page directory, and its page tables. The actual pages will be shared between the parent and child.

### trap.c

There is a new case in the switch statement for a page fault. It is wrapped in the compiler time flag `COW_FORK` so that it can be disabled along with the actual call to cowuvm() in fork(). If the flag is set, it will call the page fault handler pagefault() in `pagefault.c`. 

### pagefault.c

This file defines the function pagefault() that is used in `trap.c` to handle page faults. It does the required checks on what type of page fault occurred, and if a process attempts to write to a RO page that is marked as COW, it will perform the necessary steps. This requires checking the associated run's reference count.

If more than one process are referencing the page, it will create a copy of the page, set it to RW, and remove the COW flag. It will not modify the original page besides decrementing its reference count.

If this is the only process referencing the page, the page fault handler will set the page to RW and remove the COW flag without making any actual copies.

In both cases the entry in the TLB for that page will be invalidated so that when control is returned to the user program it will pull the updated PTE into the cache.

## Copy-On-Write Testing

### pagefault_test.c

This is the test that was required in Exercise 2 of the lab. It attempts write to a RO page which will cause a page fault. This page fault is caught in the handler, and because it is not marked as COW it will kill the process.

### refcount.c

We added a special syscall for this lab that allows us to learn a little more about any given virtual address. The syscall refcount() takes a pointer as its argument, fetches the run associated with that virtual address, and returns the runs reference count. This allowed us to write more robust unit tests. It does not give the user program access to any of the kernel's data or information beyond the number of references to that page.

### unittest.c

We built a simple framework to make writing unit tests easier. This framework includes a couple assertion macros that will terminate the unit test if the condition fails, as well as print the specified error message.

It also defines a wrapper for the unit tests that allows us to remove some redundancies in each test. The library was linked in the Makefile so that user programs can call the functions and macros from it if they include `unittest.h`.

### cowfork_test.c

This file contains the required unit tests. The first is simply a modified version of the original `forktest.c` test function, and does not count towards our number of required tests.

The second test creates a string literal that will be placed in the `.rodata` section. We then call fork() `n` times and assert that the reference count for that page equal to `n + 1`, which is the number of calls to fork() plus the parent process.

The third test creates a value, calls fork(), writes to the value, and asserts that the reference count for that page is equal to 1 since it has performed COW. It then reaps the child process and repeats `n` times to account for concurrency between the two processes.

The fourth test uses malloc() to create a page on the heap. This space will be copied on each call to fork(), but it is guaranteed that it will not be written to before we check it. This allows us to assert that the reference count is equal to 2, one for the child and one for the parent. We then write to the page and assert that the reference count has gone down to 1. This is repeated `n` times to account for concurrency.

The fifth test does a nested fork() within the child process. This results in the parent unit test, a child, and the child's child. It then asserts that a shared RO string literal has exactly 3 processes referencing it.

Each one of the tests also performs several other asserts to validate returns and that no unusual behavior has occurred.

## Header Guards

Lastly, we also added some header guards to files. This allowed us to include in a few more places without having to worry about circular inclusions.