#include "defs.h"
#include "types.h"

int sys_refcount(void) {
  char* addr;

  if(argptr(0, &addr, sizeof(char*)) < 0)
    return -1;

  return refcount(addr);
}

