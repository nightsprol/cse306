#ifndef KALLOC_H
#define KALLOC_H

#include "spinlock.h"
#include "mmu.h"
#include "memlayout.h"

#define MAXPAGES (PHYSTOP / PGSIZE)

struct run {
  struct run *next;
  //struct spinlock lock;
  volatile uint reference_count;
};

struct kmem {
  struct spinlock lock;
  struct spinlock run_lock;
  int use_lock;
  struct run *freelist;
  // Lab 2: For COW fork, we can't store the run in the
  // physical page, because we need space for the ref
  // count.  Moving the data to the kmem struct.
  struct run runs[MAXPAGES];
};

// Function headers
struct run*     v2run(char*);
struct run*     p2run(uint);
uint            increment_run(uint);
uint            decrement_run(uint);

// Kernel's memory struct
extern struct kmem kmem;

#endif
