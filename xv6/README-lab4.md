# CSE 306 Lab 4 Readme

This document gives a high-level overview of the files that were modified or added in lab 4
and the reasons behind the changes. There are a good number of comments in the actual code
itself that explains the implementation in further detail.

# Part 1: Inter-Process Shared Memory

## Implementation

Our implementation of SHM required us to modify `exec()`, `fork()`, and `cowuvm()`.
When `exec()` is called, it now sets aside `MAX_SHM` bytes after the stack for the SHM area.
This virtual address range is simply reserved for later mapping to physical pages as needed.
We added the fields `shm_start` and `shm_end` to `struct proc`.
These are now set in `exec()` once the image is being committed to.
When a process calls `fork()`, it now also copies the new fields in the proc.

During `cowuvm()`, we treated pages in the SHM area differently from the rest.
If the page is allocated, we do not make a copy, do not set it to COW, and keep it as writable.
This allows multiple processes to read and write to the SHM pages without causing a pagefault.
If the page is unallocated, it is simply skipped over similar to the unallocated stack pages.

We added the function `shmbrk()` to `proc.c` that is called by `sys_shmbrk()`.
In addition, we created the helpers `allocshm()` and `deallocshm()` to `vm.c`.
These are based on the existing functions `allocuvm()` and `deallocuvm()`.

When a user program requests to extend the SHM area with `shmbrk()`, it calls `allocshm()`.
Should any errors occur in the process, the newly allocated pages will be freed with `deallocshm()`.
If the user program requests to release the SHM area, `shmbrk()` will directly call `deallocshm()`.
If the maximum size has been reached, `shmbrk()` will return -1.
Otherwise it always returns the address of the previous end of the SHM area.

## Testing

For **Exercise 2** we wrote all of our unit tests in `shm_test.c`.
They are all listed below with an explanation of what they are testing.

- `shmbrk_get`
  - Calls `shmbrk(0)` and asserts that it returns a pointer
- `shmbrk_once`
  - Allocates a single SHM page and asserts that it increases the size by a page
- `shmbrk_to_max`
  - Allocates the maximum number of pages
  - Asserts at each step that the size has been increased by a page
- `shmbrk_beyond_max`
  - Performs the same allocations as in `shmbrk_to_max`
  - Then allocates one extra page and asserts that `shmbrk()` returns -1
- `shmbrk_release`
  - Performs the same allocations as in `shmbrk_to_max`
  - It then frees all the SHM pages and asserts that `shmbrk(0)` returns the initial address
  - Lastly it allocates once and asserts that the size has been increased by a page
- `shmbrk_write`
  - Allocates a single SHM page and attempts to write a character to it
- `shmbrk_read`
  - Allocates a single SHM page and attempts to read a character from it after writing
- `shmbrk_fork_write`
  - Allocates a single SHM page
  - In a child process, it writes and asserts that the reference count is still 2
  - After the parent reaps the child, it writes and asserts that the reference count is 1
- `shmbrk_fork_release`
  - Allocates a single SHM page
  - In a child process, it releases the page and asserts that it has been reset
  - After the parent reaps the child, it writes and asserts that the reference count is 1
- `shmbrk_fork_shmbrk`
  - Allocates a single SHM page
  - In a child process, it calls `shmbrk()` again and performs assertions
  - After the parent reaps the child, it asserts that the reference count for its page is 1
- `shmbrk_pagefault`
  - Allocates a single SHM page
  - In a child process, it writes to a page, releases it, and then attempts to write to it again
  - After the parent reaps the child, it asserts that the child exited due to a pagefault
  - The parent also writes to the SHM page and makes sure that it does not cause a pagefault
- `read_valid_shm`
  - Allocates a single SHM page
  - Creates a file and writes some string to it
  - Then it reads the file into a buffer in the SHM page
  - Lastly it asserts that `read()` successfully wrote the characters into the buffer
- `read_invalid_shm`
  - Creates a file and writes some string to it
  - Then it reads the file into an unallocated buffer in the SHM page
  - Lastly it asserts that `read()` returned -1

# Part 2: In-Kernel Support for Synchronization Primitives

## Implementation

To implement futexes, we added the functions `futex_wait()` and `futex_wake()` in `proc.c`.
The first only returns if either the value at the location no longer matches the expected value,
or if the process has been killed.
When `sleep()` is called, the location passed to `futex_wait()` is used as the channel.
To make the operation atomic, we acquire and release `ptable.lock`.
This is also the lock that is passed as the second argument to `sleep()`.

## Testing

For **Exercise 4** we wrote all of our unit tests in `futex_test.c`.
They are all listed below with an explanation of what they are testing.

- `wait_for_child`
  - This test is based on the one in `futextest.c`
  - It calls `fork()` 32 times, each time performing the same interleaving of instructions
  - This interleaving is enforced by calls to `futex_wait()` and `futex_wake()` in the child and parent
  - At each step there are assertions made to ensure that the behavior is as expected
- `wait_kill`
  - Calls `fork()` to create a child process that waits on a condition that will never happen
  - The parent then calls `sleep()` to ensure that the child process runs first
  - After sleeping, the parent calls `kill()` on the child process and reaps it
  - This test confirms that the child process hangs indefinitely

# Part 3: Locks and Condition Variables

## Implementation

For our implementation of mutexes, we modified the `mutex_t` struct to have a single volatile int.
We created the following functions in `ulib.c` for user programs to call:

- `void mutex_init(mutex_t *mutex)`
  - Initializes the `locked` field of the mutex to 0
- `void mutex_lock(mutex_t *mutex)`
  - Uses the atomic GCC `test_and_set` builtin to acquire the lock
  - On failure it calls `futex_wait()` with the address of the lock as the channel
- `int mutex_trylock(mutex_t *mutex)`
  - Uses the atomic GCC `test_and_set` builtin to acquire the lock
  - On failure it immediately returns -1, otherwise 0 if the lock was acquired
- `void mutex_unlock(mutex_t *mutex)`
  - Uses the atomic GCC `store_n` builtin to write the value of the lock
  - Calls `futex_wake()` with the address of the lock as the channel

To implement condition variables, we added a volatile int to the `cond_var_t` struct.
We created the following functions in `ulib.c` for user programs to call:

- `void cv_init(cond_var_t *cv)`
  - Initializes the `cond` field of the CV to 0
- `void cv_wait(cond_var_t *cv, mutex_t *mutex)`
  - Uses the atomic GCC `load_n` builtin to get the value of the condition
  - Releases the mutex
  - Calls `futex_wait()` with the address of the condition and the value of the local variable
  - Reacquires the mutex after returning from `futex_wait()`
- `void cv_bcast(cond_var_t *cv)`
  - Uses the atomic GCC `fetch_add` builtin to increment the value of the condition
  - Calls `futex_wait()` with the address of the condition as the channel

## Testing

To test the mutex implementation made in **Exercise 5**, we wrote tests in `mutex_test.c`.
Each one is initialized with `test_init()` that calls `shmbrk()` and creates a shared data structure.

- `count_procs_no_lock`
  - Creates NPROCS child processes
  - Each child stores a local copy of the shared value and then calls `sleep()`
  - After waking up, each child sets the shared value to its local copy plus 1
  - The parent then reaps all its child processes
  - It asserts that the shared value does not match NPROCS due to race conditions
- `count_procs_lock`
  - Creates NPROCS child processes
  - Each child does the same as in the previous test but protects their accesses with the mutex
  - The parent then reaps all its child processes
  - It asserts that the shared value equals NPROCS due to protection around the critical region
- `lock_ordering`
  - This test involves a parent and a single child process
  - It is similar to the test `wait_for_child` in `futex_test.c`
  - It uses the mutex to enforce a specific ordering between the two processes
  - The shared value is repeatedly written to and read from with assertions at each step
- `trylock_succeed`
  - Asserts that calling `mutex_trylock()` returns 0 when it acquires the lock
- `trylock_fail`
  - Asserts that calling `mutex_trylock()` returns -1 immediately when it does not acquire the lock

To test our implementation of condition variables made in **Exercise 6**, we wrote a test in `cv_test.c`.

- `increment_shared_data`
  - Creates NPROCS child processes that will only increment if the shared value is even
  - Creates NPROCS child processes that will only increment if the shared value is odd
  - Each process uses the CV to block until the condition is met
  - The pattern is the same as producers/consumers but with only a single counter being modified
  - The parent then reaps all its child processes and asserts that the value is as expected

For **Exercise 7** we created a circular bounded queue in `checksum.c`.

The parent process calls `shmbrk()` and places a shared queue and checksum into the SHM area.
The parent then creates 4 producers and 4 consumers.

Each producer opens up the `README` file and calls `enqueue()` with 12 bytes at a time.
Before each producer begins to write to the buffer, they atomically increment the number of active
producers writing to the queue.
If there isn't enough room in the queue, they will wait on the queue's condition `non_full`.
When each producer is finished writing all the bytes of the file to the queue, they atomically
decrement the number of active producers and broadcast one last time that the queue is `non_empty`.

Each consumer attempts to `dequeue()` 8 bytes at a time.
If there aren't enough bytes in the queue, they will wait on the queue's condition `non_empty`.
They also must wait if the number of active producers for the queue is greater than 0.
After reading some bytes from the queue, each consumer atomically adds each byte to a shared checksum.
We also make sure that each consumer reads at least 1 byte from the queue.
This guarantees that if a consumer is scheduled before any producers, they will not early exit.

The parent process then reaps all the producers and consumers and displays the final checksum.
