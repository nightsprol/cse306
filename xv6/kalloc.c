// Physical memory allocator, intended to allocate
// memory for user processes, kernel stacks, page table pages,
// and pipe buffers. Allocates 4096-byte pages.

#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "kalloc.h"
#include "x86.h"
#include "proc.h"

void freerange(void *vstart, void *vend);
extern char end[]; // first address after kernel loaded from ELF file
                   // defined by the kernel linker script in kernel.ld

struct kmem kmem;

// Initialization happens in two phases.
// 1. main() calls kinit1() while still using entrypgdir to place just
// the pages mapped by entrypgdir on free list.
// 2. main() calls kinit2() with the rest of the physical pages
// after installing a full page table that maps them on all cores.
void
kinit1(void *vstart, void *vend)
{
  initlock(&kmem.lock, "kmem");
  initlock(&kmem.run_lock, "kmem_run");
  kmem.use_lock = 0;
  freerange(vstart, vend);
}

void
kinit2(void *vstart, void *vend)
{
  freerange(vstart, vend);
  kmem.use_lock = 1;
}

void
freerange(void *vstart, void *vend)
{
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
    kfree(p);
}
// Free the page of physical memory pointed at by v,
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
  struct run *r;

  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
    panic("kfree");

  // Acquire kmem's locks before reading or writing
  if(kmem.use_lock) {
    acquire(&kmem.run_lock);
    acquire(&kmem.lock);
  }

  // Lab2: because we moved 'runs' to kmem
  //r = (struct run*)v;
  r = &kmem.runs[(V2P(v) / PGSIZE)];

  // Don't free if other pages are referencing this page
  // Simply decrement the count and return after releasing the locks
  if(r->reference_count > 1) {
    r->reference_count -= 1;
    
    if(kmem.use_lock) {
      release(&kmem.lock);
      release(&kmem.run_lock);
    }
    return;
  }
  
  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);

  // Insert back into the free list
  r->next = kmem.freelist;
  kmem.freelist = r;

  // Release kmem's locks
  if(kmem.use_lock) {
    release(&kmem.lock);
    release(&kmem.run_lock);
  }
}

// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
  struct run *r;
  char *rv;

  if(kmem.use_lock)
    acquire(&kmem.lock);

  r = kmem.freelist;

  // Shift the head of the freelist and initialize ref count
  if(r) {
    kmem.freelist = r->next;
    r->reference_count = 1;
  }

  if(kmem.use_lock)
    release(&kmem.lock);

  // Lab2: because we moved 'runs' to kmem
  //return (char*)r;
  rv = r ? P2V((r - kmem.runs) * PGSIZE) : r;
  return rv;
}

// Get the run associated with the given virtual address
// This is intended for a user space virtual address
struct run*
v2run(char *va) {
  pte_t *pte;

  if((pte = walkpgdir(myproc()->pgdir, (void*)va, 0)) == 0) {
    return 0;
  }

  return p2run(PTE_ADDR(*pte));
}

// Get the run associated with the given physical address
struct run*
p2run(uint pa) {
  uint idx = pa / PGSIZE;
  if(idx > MAXPAGES) return 0;
  return &kmem.runs[idx];
}

// Increment the reference count of a run atomically
uint
increment_run(uint pa)
{
  struct run* r = p2run(pa);
  if(!r) return 1;
  
  __sync_fetch_and_add(&r->reference_count, 1);
  return 0;
}

// Decrement the reference count of a run atomically
uint
decrement_run(uint pa)
{
  struct run* r = p2run(pa);
  if(!r) return 1;
  
  __sync_fetch_and_sub(&r->reference_count, 1);
  return 0;
}

