#include "unittest.h"
#include "user.h"

// Lab 4 Part 3 Tests

#define PGSIZE 4096
#define NPROCS 16
#define NTIMES 16

typedef struct {
  uint value;
  cond_var_t non_odd;
  cond_var_t non_even;
  mutex_t lock;
} data_t;

data_t *data;

/*       HELPERS       */

void
init_data(void)
{
  data = (data_t*) shmbrk(PGSIZE);
  data->value = 0;
  cv_init(&data->non_odd);
  cv_init(&data->non_even);
  mutex_init(&data->lock);
}

void
increment_odd(data_t *data)
{
  mutex_lock(&data->lock);
  while(data->value % 2 == 0)
    cv_wait(&data->non_odd, &data->lock);

  data->value++;
  assert(data->value % 2 == 0, "");
  
  cv_bcast(&data->non_even);
  mutex_unlock(&data->lock);
}

void
increment_even(data_t *data)
{
  mutex_lock(&data->lock);
  while(data->value % 2)
    cv_wait(&data->non_even, &data->lock);

  data->value++;
  assert(data->value % 2, "");
  
  cv_bcast(&data->non_odd);
  mutex_unlock(&data->lock);
}

/*     UNIT TESTS      */

void
increment_shared_data(void)
{
  int i, stat;

  for(i = 0; i < NPROCS; i++) {
    if(!fork()) {
      for(int j = 0; j < NTIMES; j++)
        increment_even(data);
      exit();
    }

    if(!fork()) {
      for(int j = 0; j < NTIMES; j++)
        increment_odd(data);
      exit();
    }
  }

  for(i = 0; i < NPROCS * 2; i++) {
    assert(waitstat(&stat) >= 0, "wait did not get all children");
    assert(stat == 0, "child process exited with %d", stat);
  }
  
  assert(data->value == NPROCS * NTIMES * 2,
         "value was %d, expected %d", data->value, NPROCS * NTIMES * 2);
}

int
main(int argc, char *argv[]) {
  start_tests(argc, argv);

  Test(increment_shared_data, .init=init_data);
  
  end_tests();
}
