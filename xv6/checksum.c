#include "user.h"
#include "fcntl.h"

#define NUM_PROCS      4
#define BUFFER_SIZE    128
#define PRODUCER_LEN   12
#define CONSUMER_LEN   8

void producer(void);
void consumer(void);

typedef struct {
  char *buffer;          // Base address of the queue

  uint capacity;         // Capacity of the queue in bytes
  uint size;
  uint head, tail;       // Pointers to front and rear of queue

  volatile uint producers; // Counts number of producers using the queue
  
  cond_var_t non_empty;
  cond_var_t non_full;
  mutex_t lock;          // Protects the queue
} queue_t;

queue_t *queue;
volatile int *checksum;

void
init_queue(queue_t *queue, char *buffer, uint capacity)
{
  queue->buffer = buffer;
  queue->capacity = capacity;
  queue->size = 0;
  queue->head = 0;
  queue->tail = 0;

  queue->producers = 0;
  
  cv_init(&queue->non_empty);
  cv_init(&queue->non_full);
  mutex_init(&queue->lock);
}

void
start_producer(queue_t *queue)
{
  __atomic_fetch_add(&queue->producers, 1, __ATOMIC_SEQ_CST);
}

void
stop_producer(queue_t *queue)
{
  __atomic_fetch_sub(&queue->producers, 1, __ATOMIC_SEQ_CST);
  cv_bcast(&queue->non_empty);
}

void
enqueue(queue_t *queue, char *data, uint len)
{
  mutex_lock(&queue->lock);
  while(queue->size + len > queue->capacity)
    cv_wait(&queue->non_full, &queue->lock);
  
  for(uint i = 0; i < len; i++) {
    queue->buffer[(queue->tail++) % queue->capacity] = data[i];
    queue->tail = queue->tail % queue->capacity;
  }

  queue->size += len;
  
  cv_bcast(&queue->non_empty);
  mutex_unlock(&queue->lock);
}

uint
dequeue(queue_t *queue, char *data, uint len)
{
  mutex_lock(&queue->lock);
  while(queue->size == 0 && queue->producers > 0)
    cv_wait(&queue->non_empty, &queue->lock);

  uint i;
  for(i = 0; i < len && queue->size > 0; i++, queue->size--) {
    data[i] = queue->buffer[(queue->head++) % queue->capacity];
    queue->head = queue->head % queue->capacity;
  }
  
  cv_bcast(&queue->non_full);
  mutex_unlock(&queue->lock);

  return i;
}

int
main(void)
{
  int i;
  char *shm;
  char *buffer;
  
  shm = (char*) shmbrk(4096);

  queue = (queue_t*) shm;
  checksum = (int*) (queue + 1);
  buffer = (char*) (checksum + 1);
  init_queue(queue, buffer, BUFFER_SIZE);
  
  for(i = 0; i < NUM_PROCS; i++) {
    if(!fork()) producer();
    if(!fork()) consumer();
  }

  for(i = 0; i < NUM_PROCS * 2; i++)
    wait();

  printf(1, "Checksum: %d\n", *checksum);
  exit();
}

void
producer(void)
{
  int fd, len;
  char buffer[PRODUCER_LEN];

  fd = open("README", O_RDONLY);
  
  start_producer(queue);
  while((len = read(fd, buffer, PRODUCER_LEN)) > 0)
    enqueue(queue, buffer, len);
  stop_producer(queue);

  close(fd);
  exit();
}

void
consumer(void)
{
  int len, i;
  uint consumed = 0;
  char buffer[CONSUMER_LEN];
  
  while((len = dequeue(queue, buffer, CONSUMER_LEN)) > 0 || !consumed) {
    for(i = 0; i < len; i++) {
      __atomic_fetch_add(checksum, (int) buffer[i], __ATOMIC_SEQ_CST);
      consumed = 1;
    }
  }

  exit();
}
