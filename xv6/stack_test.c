#include "unittest.h"
#include "user.h"

#define MAX_STACK  0x100000
#define PGSIZE     0x001000
#define EXT_SIZE   PGSIZE - 1

// Lab 3 Part 2 Tests

/*           HELPERS            */

// Write n chars to an array
static void
write_to_array(char array[], uint n)
{
  for(int i = 0; i < n; i++)
    array[i] = (char)(i % 26) + 97;
}

// Initial n should be roughly the number of pages you wish to extend
// the stack by. It will call recursively to extend to that point.
static void
alloc_arrays(uint n)
{
  char array[EXT_SIZE];
  write_to_array(array, EXT_SIZE);

  array[0] = 0;     // null-terminate the string
  printf(1, array); // print just the null-terminator so it reads
  
  if(n > 0)
    alloc_arrays(n - 1);
}

// Assert that the page for *va has n references
static void
assert_refcount(char *va, int n)
{
  assert(refcount(va) == n, "Refcount was %d, should be %d", refcount(va), n);
}

/*         UNIT TESTS          */

// Lab 3 Exercises 3 and 5 Unit Tests

void
overflow_first_page(void)
{
  alloc_arrays(2); // Unit test wrapper will assert that the test exits with EXIT_SUCCESS
}

void
overflow_to_max_pages(void)
{
  alloc_arrays(240); // Unit test wrapper will assert that the test exits with EXIT_SUCCESS
}

void
overflow_beyond_max_pages(void)
{
  alloc_arrays(1000); // Unit test wrapper will assert that the test exits with T_PGFLT
}

// Lab 3 Exercise 6 Unit Tests

void
read_unallocated_stack_page(void)
{
  char c = 'A';
  char *ptr = &c + 2*PGSIZE;

  printf(1, "%s\n", ptr);
}

void
write_unallocated_stack_page(void)
{
  char c = 'A';
  char *ptr = &c + 2*PGSIZE;

  assert(wolfie(ptr, PGSIZE) < 0, "wolfie() syscall allowed a user prog to pass an invalid pointer");
}

// Lab 3 Exercise 7 Unit Tests

void
stack_simple_cow(void)
{
  int stat;
  char a[EXT_SIZE];
  write_to_array(a, EXT_SIZE);

  if(!fork()) {
    a[0] = 'B';
    assert_refcount(a, 1);
    exit();
  } else {
    a[0] = 'A';
    assert(waitstat(&stat) >= 0, "wait did not find the child");
    assert(stat == 0, "child exited with %d", stat);
    assert_refcount(a, 1);
  }
}

void
stack_extend_cow(void)
{
  /*
    extend the stack
    
    create a child proc
    extend the stack
    
    assert that first page of stack has 2 references
    assert that second page of stack has 1 reference
  */
  int stat = 0;
  uint SIZE = EXT_SIZE;
  
  char a0[SIZE];
  char a1[SIZE];
  char a2[SIZE];
  write_to_array(a0, SIZE);
  write_to_array(a1, SIZE);
  write_to_array(a2, SIZE);
  
  if(!fork()) {
    char a3[SIZE];
    write_to_array(a3, SIZE);

    assert_refcount(a0, 2);
    assert_refcount(a3, 1);

    exit();
  } else {
    assert(waitstat(&stat) >= 0, "did not wait for child");
    assert(stat == 0, "child exited with %d", stat);
  }
}

void
stack_extend_to_max_cow(void)
{
  /*
    extend to bottom of stack
    
    fork
    assert refcount is 2 for all pages
    exit in child
    
    assert refcount in parent for each page is 1 after waiting
  */
  int stat;
  uint N = 220, SIZE = EXT_SIZE;
  char *arrays[N];

  for(int i = 0; i < N; i++) {
    char a[SIZE];
    write_to_array(a, SIZE);
    arrays[i] = a;
  }

  if(!fork()) {
    for(int i = 0; i < N; i++)
      assert_refcount(arrays[i], 2);
    exit();
  } else {
    assert(waitstat(&stat) >= 0, "wait did not find the child");
    assert(stat == 0, "child exited with %d", stat);
  }

  for(int i = 0; i < N; i++)
    assert_refcount(arrays[i], 1);
}

void
stack_extend_interleave(void)
{
  /*
    repeat N times
      fork four times
      all children attempt to write to the page guard at the same time 
      wait to reap all children

    the goal is test interleaving of stack extending
    obviously can't be guaranteed but that's why it runs N times
  */
  int stat;
  uint N = 32, SIZE = EXT_SIZE, FORKS = 8;
  
  for(int i = 0; i < N; i++) {
    int j;
    
    for(j = 0; j < FORKS; j++) {
      if(!fork()) {
        char a0[SIZE];
        char a1[SIZE];
        write_to_array(a0, SIZE);
        write_to_array(a1, SIZE);

        assert_refcount(a1, 1);
        exit();
      }
    }

    for(; j > 0; j--) {
      assert(waitstat(&stat) >= 0, "wait stoppped early");
      assert(stat == 0, "child exited with %d", stat);
    }
  }

  assert_refcount((char*)&stat, 1);
}

int
main(int argc, char *argv[]) {
  start_tests(argc, argv);

  Test(overflow_first_page);
  Test(overflow_to_max_pages);
  Test(overflow_beyond_max_pages, .exit_code=14); // Assert that it crashes on a page fault

  Test(read_unallocated_stack_page, .exit_code=14); // Assert that it crashes on a page fault
  Test(write_unallocated_stack_page);
  
  Test(stack_simple_cow);
  Test(stack_extend_cow);
  Test(stack_extend_to_max_cow);
  Test(stack_extend_interleave);
  
  end_tests();
}
