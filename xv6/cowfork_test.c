#include "unittest.h"
#include "user.h"

#define N  1000
#define PGSIZE 4096

// Lab 2 Tests

/*           HELPERS            */

static void
assert_refcount(char *va, int count)
{
  assert(refcount(va) == count, "Refcount was %d, should be %d", refcount(va), count);
}

/*         UNIT TESTS          */

void
n_forks(void)
{
  int n, pid;

  for(n=0; n<N; n++) {
    pid = fork();

    if(pid < 0)
      break;
    
    if(pid == 0)
      exit();
  }

  assert(n != N, "fork claimed to work N times!");
  
  for(; n > 0; n--)
    assert(wait() >= 0, "wait stopped early");

  assert(wait() == -1, "wait got too many");
}

void
read_only_fault(void)
{

  char *s = "_0123456789ABCDEFG_";
  int n, pid;
  
  for(n = 0; n < N; n++) {
    pid = fork();
    
    if(pid < 0)
      break;
    
    if(!pid)
      exit();
  }

  assert(n != N, "fork claimed to work N times!");
  assert_refcount(s, n + 2);
  
  for(; n > 0; n--)
    assert(wait() >= 0, "wait stopped early");
  
  assert(wait() == -1, "wait got too many");
}

void
write_fault(void)
{
  const int NUM = 100;

  int n, pid;
  char value = 'A';
  
  for(n = 0; n < NUM; n++) {
    pid = fork();

    assert(pid >= 0, "PID was %d after calling fork()", pid);

    if(!pid) {
      value = 'B';

      assert_refcount(&value, 1);
      exit();
    } else {
      assert(wait() >= 0, "wait stopped early");
    }
  }

  assert(n == NUM, "fork only worked %d times instead of %d", n, NUM);
  assert_refcount(&value, 1);
  assert(wait() == -1, "wait got too many");
}

void
create_page(void)
{
  const int NUM = 100;

  char* page = (char*) malloc(PGSIZE);
  int n, pid;

  for(n = 0; n < NUM; n++) {
    pid = fork();

    assert(pid >= 0, "PID was %d after calling fork()", pid);
    
    if(!pid) {
      assert_refcount(page, 2);

      *page = 'A';

      assert_refcount(page, 1);
      exit();
    } else {
      assert(wait() >= 0, "wait stopped early");
    }
  }

  assert(n == NUM, "fork only worked %d times instead of %d", n, NUM);
  assert_refcount(page, 1);
  assert(wait() == -1, "wait got too many");
  
  free((void*) page);
}

void
fork_in_child(void)
{
  char* str = "_asdfasdf0923095al_";
  int pid = fork();
  if(!pid) {
    int pid2 = fork();
    if(!pid2) {
      // child of child
      assert_refcount(str, 4);
      
      exit();
    } else {
      assert(wait() >= 0, "wait stopped early");
      exit();
    }
  } else {
    assert(wait() >= 0, "wait stopped early");
  }

  assert_refcount(str, 2);
  assert(wait() == -1, "wait got too many");
}

int main(int argc, char *argv[]) {
  start_tests(argc, argv);

  Test(n_forks);
  Test(read_only_fault);
  Test(write_fault);
  Test(create_page);
  Test(fork_in_child);
  
  end_tests();
}
