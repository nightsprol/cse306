#include "unittest.h"
#include "user.h"
#include "fcntl.h"

// Lab 4 Part 1 Tests

#define PGSIZE 4096
#define MAX_SHM 0x1000000

/*       HELPERS       */

// Assert that the page for *va has n references
static void
assert_refcount(char *va, int n)
{
  assert(refcount(va) == n, "Refcount was %d, should be %d", refcount(va), n);
}

/*     UNIT TESTS      */

// Get the end of the SHM without allocating
void
shmbrk_get(void)
{
  assert(shmbrk(0), "");
}

// Allocate 1 page and get the end
void
shmbrk_once(void)
{
  uint end = shmbrk(0);
  assert(shmbrk(1) == end, "");
  assert(shmbrk(0) == end + PGSIZE, "");
}

// Allocate max number of pages
void
shmbrk_to_max(void)
{
  uint end = shmbrk(0);

  for(int i = 0; i < MAX_SHM; i += PGSIZE) {
    assert(shmbrk(1) == end, "");
    assert(shmbrk(0) == end + PGSIZE, "");
    end = shmbrk(0);
  }
}

// Attempt to allocate the max number of pages + 1
void
shmbrk_beyond_max(void)
{
  shmbrk_to_max();
  assert(shmbrk(1) == -1, "");
}

// Allocate max number of pages and release them all
void
shmbrk_release(void)
{
  uint end = shmbrk(0);
  shmbrk_to_max();
  assert(shmbrk(-1) > 0, "");
  assert(shmbrk(0) == end, "");
  assert(shmbrk(1) == end, "");
  assert(shmbrk(0) == end + PGSIZE, "");
}

// Write to an allocated SHM page
void
shmbrk_write(void)
{
  char *end = (char*)shmbrk(1);
  *end = 'A';
}

// Read from an allocated SHM page
void
shmbrk_read(void)
{
  char *end = (char*)shmbrk(1);
  *(end+1) = 'A';
  *end = 0;
  printf(1, end);
}

// Allocate an SHM page, fork, and write to it
void
shmbrk_fork_write(void)
{
  int stat;
  char *end = (char*)shmbrk(1);

  if(!fork()) {
    assert_refcount(end, 2);
    *end = 'A';
    assert_refcount(end, 2);
  } else {
    assert(waitstat(&stat) >= 0, "wait did not find the child");
    assert(stat == 0, "child exited with %d", stat);
    assert(*end == 'A', "end had %c instead of A", *end);
    assert_refcount(end, 1);
  }
}

// Allocate an SHM page, fork, and then release it
void
shmbrk_fork_release(void)
{
  int stat;
  char *end = (char*)shmbrk(1);

  if(!fork()) {
    assert(shmbrk(-1) > 0, "");
    assert((char*)shmbrk(0) == end, "");
  } else {
    assert(waitstat(&stat) >= 0, "wait did not find the child");
    assert(stat == 0, "child exited with %d", stat);
    *end = 'A';
    assert_refcount(end, 1);
  }
}

// Allocate an SHM page, fork, then allocate another
void
shmbrk_fork_shmbrk(void)
{
  int stat;
  char *end = (char*)shmbrk(1);

  if(!fork()) {
    assert_refcount(end, 2);
    end = (char*)shmbrk(0);
    assert((char*)shmbrk(1) == end, "");
    assert((char*)shmbrk(0) == end + PGSIZE, "");
    assert_refcount(end, 1);
  } else {
    assert(waitstat(&stat) >= 0, "wait did not find the child");
    assert(stat == 0, "child exited with %d", stat);
    assert_refcount(end, 1);
  }
}

// Write to a released SHM page in a child proc
void
shmbrk_pagefault(void)
{
  int stat;
  char *end = (char*)shmbrk(1);

  if(!fork()) {
    *end = 'A';
    shmbrk(-1);
    *end = 'B'; // Should cause a pagefault
  } else {
    assert(waitstat(&stat) >= 0, "wait did not find the child");
    assert(stat == 14, "child exited with %d, expected 14 (pagefault)", stat);
    assert_refcount(end, 1);

    assert(*end == 'A', "");
    *end = 'C'; // Should not cause a pagefault
  }
}

// Use the read() syscall with a valid SHM address
void
read_valid_shm(void)
{
  char *end = (char*)shmbrk(1);
  int fd;

  fd = open("read_valid", O_CREATE|O_WRONLY);
  assert(fd >= 0, "");
  assert(write(fd, "ababababab", 10) == 10, "");
  close(fd);

  fd = open("read_valid", O_RDONLY);
  assert(fd >= 0, "");
  assert(read(fd, end, 10) == 10, "");
  close(fd);

  assert(unlink("read_valid") >= 0, "");
}

// Use the read() syscall with an invalid SHM address
void
read_invalid_shm(void)
{
  shmbrk(1);

  char *end = (char*)shmbrk(0);
  int fd;

  fd = open("read_invalid", O_CREATE|O_WRONLY);
  assert(fd >= 0, "");
  assert(write(fd, "ababababab", 10) == 10, "");
  close(fd);

  fd = open("read_invalid", O_RDONLY);
  assert(fd >= 0, "");
  assert(read(fd, end, 10) == -1, "");
  close(fd);

  assert(unlink("read_invalid") >= 0, "");
}

int
main(int argc, char *argv[])
{
  start_tests(argc, argv);

  Test(shmbrk_get);
  Test(shmbrk_once);
  Test(shmbrk_to_max);
  Test(shmbrk_beyond_max);
  Test(shmbrk_release);
  Test(shmbrk_write);
  Test(shmbrk_read);
  Test(shmbrk_fork_write);
  Test(shmbrk_fork_release);
  Test(shmbrk_fork_shmbrk);
  Test(shmbrk_pagefault);
  Test(read_valid_shm);
  Test(read_invalid_shm);
  
  end_tests();
}
