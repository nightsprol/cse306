#include "unittest.h"
#include "user.h"

// Lab 4 Part 2 Tests

void
wait_for_child(void) {
  int i, v, stat;
  int *sync = (int*) shmbrk(4096);
  *sync = 0;

  for(i = 0; i < 32; i++) {
    v = *sync;
    
    if(!fork()) {
      futex_wait(sync, v+1);    // Passes
      assert(*sync == v, "");   // 1st
      *sync = *sync + 1;
      futex_wake(sync);         // Wakeup parent
    
      futex_wait(sync, v+1);    // Sleeps
      assert(*sync == v+2, ""); // 3rd

      // Exit child proc
      exit();
    } else {
      futex_wait(sync, v);      // Sleeps
      assert(*sync == v+1, ""); // 2nd
      *sync = *sync + 1;
      futex_wake(sync);         // Wakeup child

      // Reap child proc
      assert(waitstat(&stat) >= 0, "wait did not find the child");
      assert(stat == 0, "child exited with %d", stat);
    }
  }
}

void
wait_kill(void)
{
  int pid, stat;
  int *sync = (int*) shmbrk(4096);
  *sync = 4;
  
  if(!(pid = fork())) {
    futex_wait(sync, 4);
    assert(0, "this shouldn't happen");
  }

  sleep(32);
  kill(pid);
  assert(waitstat(&stat) >= 0, "wait did not find the child");
  assert(stat == 64, "child exited with %d", stat);
}

int
main(int argc, char *argv[]) {
  start_tests(argc, argv);

  Test(wait_for_child);
  Test(wait_kill);
  
  end_tests();
}
