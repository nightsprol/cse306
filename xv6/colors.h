#ifndef COLORS_H
#define COLORS_H

#define BOLD "\033[1;1m"
#define UNDR "\033[1;2m"
#define BLNK "\033[1;3m"

#define KNRM "\033[0m"
#define KRED "\033[1;31m"
#define KGRN "\033[1;32m"
#define KYEL "\033[1;33m"
#define KBLU "\033[1;34m"
#define KMAG "\033[1;35m"
#define KCYN "\033[1;36m"
#define KWHT "\033[1;37m"
#define KBWN "\033[0;33m"

#endif /* HEADER GUARD */
