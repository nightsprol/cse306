#include "types.h"
#include "defs.h"

int sys_wolfie(void) {
  char* buf;
  int size;

  if(argint(1, &size) < 0 || argptr(0, &buf, size) < 0)
    return -1;

  return wolfie(buf, (uint) size);
}
