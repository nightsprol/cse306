#include "unittest.h"
#include "user.h"

// Lab 4 Part 3 Tests

#define PGSIZE 4096
#define NPROCS 50

typedef struct {
  uint counter;
  mutex_t *lock;
} data_t;

data_t *data;

/*       HELPERS       */

void
test_init(void)
{
  char *p = (char*) shmbrk(PGSIZE);
  memset(p, 0, PGSIZE);
  
  mutex_t *mutex = (mutex_t*) p;
  mutex_init(mutex);
  
  data = (data_t*) (mutex + 1);
  data->counter = 0;
  data->lock = mutex;
}

/*     UNIT TESTS      */

void
count_procs_no_lock(void)
{
  int i;

  for(i = 0; i < NPROCS; i++) {
    if(!fork()) {
      int local = data->counter;
      sleep(1);
      data->counter = local + 1;
      exit();
    }
  }

  for(i = 0; i < NPROCS; i++) wait();
  assert(data->counter != NPROCS, "counter should not have been %d", NPROCS);
}

void
count_procs_lock(void)
{
  int i;

  for(i = 0; i < NPROCS; i++) {
    if(!fork()) {
      mutex_lock(data->lock);
      int local = data->counter;
      sleep(1);
      data->counter = local + 1;
      mutex_unlock(data->lock);
      exit();
    }
  }

  for(i = 0; i < NPROCS; i++) wait();
  assert(data->counter == NPROCS, "counter was %d, expected %d", data->counter, NPROCS);
}

void
lock_ordering(void)
{
  int stat;
  
  mutex_lock(data->lock);

  if(!fork()) {
    mutex_lock(data->lock);
    assert(data->counter == 1, "");
    data->counter = 2;
    mutex_unlock(data->lock);
    exit();
  }

  sleep(5);
  data->counter = 1;
  mutex_unlock(data->lock);
  assert(waitstat(&stat) >= 0, "");
  assert(stat == 0, "child process exited with %d", stat);
  assert(data->counter == 2, "");
}

void
trylock_succeed(void)
{
  assert(mutex_trylock(data->lock) == 0, "");
  mutex_unlock(data->lock);
}

void
trylock_fail(void)
{
  mutex_lock(data->lock);
  assert(mutex_trylock(data->lock) < 0, "");
  mutex_unlock(data->lock);
}

int
main(int argc, char *argv[]) {
  start_tests(argc, argv);

  Test(count_procs_no_lock, .init=test_init);
  Test(count_procs_lock, .init=test_init);
  Test(lock_ordering, .init=test_init);
  Test(trylock_succeed, .init=test_init);
  Test(trylock_fail, .init=test_init);
  
  end_tests();
}
