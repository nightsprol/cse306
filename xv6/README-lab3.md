# CSE 306 Lab 3 Readme

This document gives a high-level overview of the files that were modified or added in lab 3
and the reasons behind the changes. There are a good number of comments in the actual code
itself that explains the implementation in further detail.

# Part 1: Null Pointer Dereference

## Implementation

For part 1 we called `mappages()` in `exec()` to map the virtual address range of 0 to PGSIZE
to a non-present page. Because any user program including `sh` will `fork()` and `exec()`
from `init`, every process except `init` will have this page. After allocating and mapping
the page, we clear the present bit from its PTE. This will force it to always trap into the
page fault handler where the process will be killed.

We modified `cowuvm()` so that it treats a virtual address of 0 as a special case. Normally
the present bit in the PTE is checked and a panic occurs if it's not set. We just added an
extra check so that if the VA being copied is 0, it will override this check.

In addition, we changed the existing virtual address checks in `syscall.c` and added a couple
more checks as needed.

## Testing

For **Exercise 1** we wrote the user programs `np_read.c` and `np_write.c`. As expected they
were initially running to completion. After finishing our implementation for part 1, both
of the test programs are now terminated due to a page fault.

# Part 2: Automatic Stack Growth

## Implementation

To reserve a virtual address range of MAX_STACK + PGSIZE for a variable sized stack, we
modified where `exec()` allocated the initial stack and guard page. Instead of mapping their
virtual addresses to be immediately above the program's data, it now starts at MAX_STACK +
PGSIZE. It still only calls `allocuvm()` for two pages, however.

To keep track of the variable stack of each process, we added three new fields to the `struct
proc` defined as `char *stack_start, *stack_guard, *stack_end`. These represent the base addresses
of the first page of the stack, the stack guard, and the last available page in a fully-extended
stack. In our implementation, the heap will continue to grow upwards from the stack.

Initially the unallocated range between the program and the top of its stack is left unmapped.
When a page fault occurs, we now check if the virtual address requested falls within the page
guard's address range. If it does, we perform the necessary checks and steps required to
extend the stack. The main steps taken consist of adding the user bit to the stack's current
guard page to allow writing, invalidating its entry in the TLB, and shifting the process's
stack guard down a page. After this we call `kalloc()` to acquire a new page of physical
memory and map the new page to the virtual address range directly below the old stack guard.

We also modified `cowuvm()` so that it skips over the virtual address range for the unallocated
portion of the stack. This ensures that only pages with data are actually copied for child
processes on `fork()`.

Lastly we made some changes to `syscall.c` where user-provided virtual addresses are checked.

## Testing

All of our tests for part 2 are in the user program `stack_test.c`. 

For **Exercise 3** we wrote the test `overflow_first_page` that would initially crash before
the changes for part 2 were made. Now that the stack extends, it successfully runs to the end.

For **Exercise 5** we wrote the tests `overflow_to_max_pages` and `overflow_beyond_max_pages`
that test extending the stack almost to the maximum size and just beyond the max size,
respectively. For the second one, we assert that the process is killed due to a page fault.

For **Exercise 6** we wrote two unit tests. Both attempt to pass a pointer to an unallocated
stack page to a system call. `read_unallocated_stack_page` passes a pointer to `printf()`
and asserts that the process is killed due to a page fault. `write_unallocated_stack_page`
passes a pointer to `wolfie()` and asserts that it returns a negative value.

For **Exercise 7** we added the tests `stack_simple_cow`, `stack_extend_cow`,
`stack_extend_to_max_cow`, and `stack_extend_interleave`. The first was to test very basic
`fork()` functionality after extending the stack once. The second extends the stack by
a couple pages, calls `fork()`, and asserts that the new pages have the correct reference
count after also extending in the child. The third extends to the maximum length and then
calls `fork()` and asserts that all the stack pages have a reference count of 2. The last
test attempts to interleave stack extending in multiple child processes and asserts that
the state of the stack for both the children and parent are as expected.

# Part 3: Virtual Dynamic Shared Objects

## Implementation

We mapped the pages as needed in `allocvdso()` and incremented the reference counts as
well. For `vdso_pid_page` we also wrote the new process' PID.

## Testing

All of our tests for **Exercise 10** are in the file `vdso_test.c`.

The first two tests `getpid_fork` and `increasing_ticks` are the ones suggested in the
exercise description. The first asserts that the PID returned by the VDSO function matches
the one returned by the system call `getpid()`. The second one calls `sleep()` in a loop
and asserts that `vdso_getticks()` always increases from the last call.

We also wrote an additional test called `get_ticks_after_fork` that calls `fork()` to
create a child process. In both the child and the parent, we perform the same checks
as in the `increasing_ticks` unit test before the parent reaps its child.

# Other Changes

Our OS had started to run out of disk space because of the number of user programs we have.
We also have a unit test library that is being linked for three of our test programs. These
caused `usertests` to fail on the bigfiles test due to running out of blocks to allocate. We
just increased the file system size in `param.h` from 1000 to 2000 blocks.
