#include "defs.h"
#include "kalloc.h"
#include "memlayout.h"
#include "types.h"
#include "syscall.h"

// TODO: Remove extra header includes

// TODO: Check permission level of virtual address

int
refcount(char* va) {  
  if(v2run(va))
    return v2run(va)->reference_count;
  return -1;
}
