#include "types.h"
#include "syscall.h"
#include "user.h"

// Lab 2 Exercise 2

// This test will result in a page fault that
// will trap into our page fault handler.
int
main(void)
{
  char *buf = (char*) malloc(sizeof(char));

  for(int i = 0; i < 10; i++) {
    buf += 4096;
    *buf = 'A';
    *(buf + 1) = 0;
    printf(1, "%s\n", buf);
  }
  
  free(buf);
  exit();
}
