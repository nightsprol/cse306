#include "unittest.h"
#include "user.h"

// Lab 3 Part 3 Tests

void
getpid_fork(void) {
  int pid = fork();
  if(!pid) {
    int syspid = getpid();
    int vdsopid = vdso_getpid();
    assert(syspid == vdsopid, "getpid() %d did not match vdso_getpid(): %d\n", syspid, vdsopid);
    exit();
  }
  wait();
}

void
increasing_ticks(void) {
  int tick = vdso_getticks();
  int initialtick = tick;
  for(int i = 0; i < 100; i++) {
    sleep(1);
    int newtick = vdso_getticks();
    assert(newtick > tick, "new tick %d was not greater than old tick %d", newtick, tick);
    tick = newtick;
  }
  assert(tick > initialtick, "Tick never increased");
}

void
get_ticks_after_fork(void) {
  int pid = fork();
  if(!pid) {
    increasing_ticks();
    exit();
  }
  increasing_ticks();
  wait();
}

int
main(int argc, char *argv[]) {
  start_tests(argc, argv);

  Test(getpid_fork);
  Test(increasing_ticks);
  Test(get_ticks_after_fork);

  end_tests();
}
